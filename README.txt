Zum Bauen dieses Projektes sind ein C++-Compiler (mindestens C++-14), git und CMake (mindestens Version 3.10) notwendig.

```
git clone --recurse-submodules https://gitlab.amd.e-technik.uni-rostock.de/es-praktikum/systemc-exercise.git
cd project && mkdir build
cmake ../
cmake --build .
```
