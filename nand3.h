#pragma once

#include "systemc.h"

SC_MODULE(nand3) {
public:
	sc_in<bool> in1;
	sc_in<bool> in2;
	sc_in<bool> in3;
	sc_out<bool> out;

	SC_CTOR(nand3) {
		SC_THREAD(onChange);
		...
	}

private:
	void onChange() {
		while(true) {
			out.write(...);
		}
	}
};