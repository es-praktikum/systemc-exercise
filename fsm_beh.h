#pragma once
#include "systemc.h"

SC_MODULE(fsm_beh) {
public:
	sc_in<bool> x1;
	sc_in<bool> x2;
	sc_out<bool> y1;
	sc_out<bool> y2;

	sc_in_clk clk;

	bool q1, q2, q3;

	SC_CTOR(fsm_beh) : q1(false), q2(false), q3(false){
		SC_METHOD(go);
		...
	}
private:
	void go() {
		y1.write(...);
		y2.write(...);
	}
};
