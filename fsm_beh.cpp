#include "fsm_beh.h"

int sc_main(int argc, char* agrv[]) {
	fsm_beh fsm_beh("FSM");
	sc_clock clk("Clock", 10, SC_NS, 0.5, 1, SC_NS);
	...
	return 1;
}