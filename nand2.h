#pragma once

#include "systemc.h"

SC_MODULE(nand2){
public:
	sc_in<bool> in1;
	sc_in<bool> in2;
	sc_out<bool> out;

	SC_CTOR(nand2) {
		SC_THREAD(onChange)
			...
	}

private:
	void onChange(){
		while(true) {
			out.write(...);
		}
	}
};