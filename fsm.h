#pragma once

#include "systemc.h"

SC_MODULE(fsm) {
public:
	sc_in<bool> x1;
	sc_in<bool> x2;
	sc_out<bool> y1;
	sc_out<bool> y2;
	
	sc_in_clk clk;

	SC_CTOR(fsm){
		SC_METHOD(out);
		...
	}
private:
	void out(void){
		y1.write(...);
		y2.write(...);
	}
};
