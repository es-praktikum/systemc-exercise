#pragma once

#include "systemc.h"

SC_MODULE(dff) {
public:
	sc_in<bool> d;
	sc_in<bool> clk;
	sc_out<bool> q;
	
	SC_CTOR(dff){
		SC_METHOD(flip);
		...
	}

private:
	void flip() {
		...
	}
};